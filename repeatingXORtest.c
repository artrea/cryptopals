 #include <stdio.h>
#include <stdlib.h>
#include "cryptutil.h"

int main(int argc, char *argv[])
{
  char *string1 = malloc(BUFFSIZE*sizeof(char));
  char *tmpbuff = malloc(BUFFSIZE*sizeof(char));
  unsigned char *byte1 = malloc(BUFFSIZE*sizeof(unsigned char));
  unsigned char *byte2 = malloc(BUFFSIZE*sizeof(unsigned char));
  unsigned int bufflen;
  unsigned int bytelen;

  while( ( bufflen = readTextLine(string1, BUFFSIZE)) )
    {
      bytelen = decodeASCII(string1, byte1, BUFFSIZE, bufflen);
      char key[3] = {'I','C','E'};
      repeatingKeyXOR(byte1, key, bytelen, 3);
      encodeBase16(byte1, tmpbuff, bytelen);
      splitline(tmpbuff, 75, BUFFSIZE);
      printf("output = %s\n",tmpbuff);
    }
  free(string1);
  free(tmpbuff);
  free(byte1);
  free(byte2);

  return 0;
}
