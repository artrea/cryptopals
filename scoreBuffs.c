 #include <stdio.h>
#include <stdlib.h>
#include "cryptutil.h"

int main(int argc, char *argv[])
{
  char *string1 = malloc(BUFFSIZE*sizeof(char));
  char *tmpbuff = malloc(BUFFSIZE*sizeof(char));
  unsigned char *byte1 = malloc(BUFFSIZE*sizeof(unsigned char));
  unsigned char *byte2 = malloc(BUFFSIZE*sizeof(unsigned char));
  int bufflen,i;
  unsigned char j;
  int i2,lineNumber = 0;
  char keychar = '\0';
  double bestScore,currScore;

  while( ( bufflen = readTextLine(string1, BUFFSIZE)) )
    {
      bestScore = currScore = -99999999999999;
      decodeBase16(string1, byte1, BUFFSIZE, bufflen);
      for(i2=0;i2<256;i2++)
	{
	  j = i2;
	  for(i=0;i<BUFFSIZE;i++)
	    byte2[i] = (byte1[i]==0) ? 0 : byte1[i] ^ j;
	  encodeASCII(byte2, BUFFSIZE, tmpbuff);
	  currScore = scoreBuffer(tmpbuff, bufflen);
	  reverse(tmpbuff);
	  if( currScore > bestScore )
	    {
	      bestScore = currScore;
	      keychar = j;
	    }
	}
      if( bestScore > 10 )
	{
	  for(i=0;i<BUFFSIZE;i++)
	    byte1[i] ^= (byte1[i]==0) ? 0 : keychar;
	  encodeASCII(byte1, BUFFSIZE, tmpbuff);
	  reverse(tmpbuff);
	  printf("%s\n",tmpbuff);
	  printf("Score = %10.10f | ",bestScore);
	  printf("Line = %i | ", lineNumber);
	  printf("keychar = %c\n",keychar);
	}
      lineNumber++;
    }
  free(string1);
  free(tmpbuff);
  free(byte1);
  free(byte2);

  return 0;
}
