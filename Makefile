CC=gcc
CFLAGS=-c -Wall -g3 -O0 -std=c99
LDFLAGS=
SOURCES=cryptutil.c  repeatingXORtest.c
OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=repeatingXORtest

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.c.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -rf $(OBJECTS)
	rm -rf $(EXECUTABLE)
