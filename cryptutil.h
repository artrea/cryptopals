#ifndef CRYPTUTIL_H_
#define CRYPTUTIL_H_


#define BUFFSIZE 512
#define NUMSIZE 256


enum caps {NOCASE, CASE};

unsigned char hex2int(char c);
char uint2char64(unsigned int i);
void reverse(char *s);
void trimzeros(char *s,char zerochar);
unsigned int decodeBase16(const char *s, unsigned char *destbuff, unsigned int len, unsigned int inputlen);
void encodeBase64(const unsigned char *input, char *output, unsigned int len);
unsigned int readTextLine(char *buff, unsigned int maxlen);
char uint2char16(unsigned int num);
void encodeBase16(const unsigned char *input, char *output, unsigned int len);
double scoreBuffer(const char *input, int len);
unsigned int countFreqs(const char *input,  int *countBuff, unsigned int maxlen, unsigned int numChars);
void encodeASCII(const unsigned char *input,  int inputlength, char *output);
void repeatingKeyXOR(unsigned char *buff, const char *key, unsigned int inputlen, unsigned int keylen);
unsigned int decodeASCII(const char *input, unsigned char *destBuff, unsigned int bufflen, unsigned int inputlen);
void splitline(char *input, unsigned int dist, unsigned int inputlen);

#endif
