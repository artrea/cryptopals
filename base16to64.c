#include <stdio.h>
#include <stdlib.h>
#include "cryptutil.h"

#define BUFFSIZE 512
#define NUMSIZE 256

int main(int argc, char *argv[])
{
  char buff[BUFFSIZE];
  char output[BUFFSIZE];
  unsigned char arr[BUFFSIZE];
  int i,bufflen;
  
  for(i=0;i<BUFFSIZE;i++) arr[i] = 0; /*init byte array */

  while((bufflen=readTextLine(buff,BUFFSIZE)))
    {
      decodeBase16(buff, arr, NUMSIZE, bufflen);
      encodeBase64(arr, output, NUMSIZE);
      trimzeros(output,uint2char64(0));
      reverse(output);
      printf("%s\n",output);
    }
  return 0;
}
