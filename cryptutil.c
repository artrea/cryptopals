#include <stdio.h>
#include <stdlib.h>
#include "cryptutil.h"

unsigned char hex2int(char c)
{
  int retval = -9999999;

  if( c >= 'a' && c <= 'f' )
    retval = c - 'a' + 10;
  else if( c >= 'A' && c <= 'F' )
    retval = c - 'A' + 10;
  else if( c >= '0' && c <= '9' )
    retval = c - '0';
  else
    printf("erorr error\n");
  return retval;
}

char uint2char64(unsigned int i)
{
  char retval = 0;
  if(i < 26)
    retval = 'A' + i;
  else if(i < 52)
    retval = 'a' + i - 26;
  else if(i < 62)
    retval = '0' + i - 52;
  else if(i == 62)
    retval = '+';
  else if(i == 63)
    retval = '/';
  else
    {
      printf("error\n");
      retval = 'x';
    }
  return retval;
}

void reverse(char *s)
{
  int i,j;
  char tmp;
  for(i=0;s[i]!='\0';i++) ;
  j = i-1;
  for(i=0;i<j;i++,j--)
    {
      tmp = s[i];
      s[i] = s[j];
      s[j] = tmp;
    }
}
  
void trimzeros(char *s, char zerochar)
{
  int i;
  for(i=0;s[i]!='\0';i++) ;
  i--;
  for(;s[i]==zerochar;i--) s[i] = '\0';
}

unsigned int decodeBase16(const char *input, unsigned char *destbuff, unsigned int len, unsigned int inputlen)
{
  int i,j;
  unsigned char currbyte;
  for(i=0;i<len;i++) destbuff[i] = 0;
  for(j=0,i=inputlen-1;i>=0 && j < len;i-=2)
    {
      if(i>0)
	currbyte = 16*hex2int(input[i-1]) + hex2int(input[i]);
      else
	currbyte = hex2int(input[i]);
      destbuff[j++] = currbyte;
    }
  return j;
}

void encodeBase64(const unsigned char *input, char *output, unsigned int len)
{
  int outpos = 0;
  int read3bytes;
  int i,j,k;
  for(i=len-1;i>0;i -= 3)
    {
      read3bytes = 0;
      for(j=0;j<3 && i-j >= 0;j++)
 	{
	  read3bytes |= (input[i-j]) << (j*8);
	}
      for(k=0;k<4;k++)
	{
	  output[outpos++] = uint2char64(read3bytes % 64);
	  read3bytes >>= 6;
	}
    }
  output[outpos] = '\0';
  trimzeros(output, uint2char64(0));
  reverse(output);
}

unsigned int readTextLine(char *buff, unsigned int maxlen)
{
  int len = 0;
  char c;
  while((c=getchar()) && (c!=EOF))
    {
      buff[len++] = c;
    }
  buff[len] = '\0';
  return len;
}

char uint2char16(unsigned int num)
{
  char retval;
  if( num < 10 )
    retval = '0' + num;
  else if( num < 16 )
    retval = 'a' + num - 10;
  else
    printf("Error bad input to uint2char16\n");
  return retval;
}

void encodeBase16(const unsigned char *input, char *output, unsigned int len)
{
  int i,j;
  unsigned char currentByte;
  for(i=0,j=0; i < len; i++)
    {
      currentByte = input[i];
      output[j++] = uint2char16(currentByte >> 4);
      output[j++] = uint2char16(currentByte & 15);
    }
  output[j] = '\0';
  /* trimzeros(output, '0'); */
}
			
double scoreBuffer(const char *input, int len)
{
  const int numChars = 256;
  int *freqs = NULL;
  freqs = malloc(sizeof(int)*numChars);
  int total = countFreqs(input, freqs, len, numChars);
  double score = 1.0;
  int i;
  for(i='A';i<='Z';i++)
    {
      freqs[i-'A'+'a'] += freqs[i];
      freqs[i] = 0;
    }
  for(i=0; i<numChars; i++)
    {
      switch(i)
	{
	case(' '): score += 200.0*freqs[i]; break;
	case('e'): score += 120.0*freqs[i]; break;
	case('t'): score += 90.0*freqs[i]; break;
	case('a'): score += 80.0*freqs[i]; break;
	case('o'): score += 76.0*freqs[i]; break;
	case('i'): score += 73.0*freqs[i]; break;
	case('n'):
	case('s'):
	case('r'):
	case('h'): score += 50*freqs[i]; break;
	case('d'):
	case('l'):
	case('u'):
	case('c'):
	case('m'):
	case('f'):
	case('y'):
	case('w'):
	case('g'): score += 10*freqs[i]; break;
	case('p'):
	case('b'):
	case('v'):
	case('k'):
	case('x'):
	case('q'):
	case('j'):
	case('z'): score += 1*freqs[i]; break;
	case('.'):
	case('"'):
	case(','):
	case('!'):
	case('?'):
	case('('):
	case(')'): break;
	case('\0'): break;
	default:   score -= 100*freqs[i]; break;
	}
     }
  if(freqs!= NULL) free(freqs);
  if(total > 0) score /= (double)total;
  return score;
}

unsigned int countFreqs(const char *input,  int *countBuff, unsigned int maxlen, unsigned int numChars)
{
  int i;
  unsigned char c;
  for(i=0;i<numChars;i++) countBuff[i] = 0;
  for(i=0;i<maxlen;i++)
    {
      c = input[i];
      countBuff[c-'\0']++;
    }
  return i;
}

void encodeASCII(const unsigned char *input, int inputlength, char *output)
{
  int i = 0;
  int oi = 0;
  for(i=0; i < inputlength-1; i++)
    output[oi++] = (char) input[i];
  output[oi] = '\0';
}

void repeatingKeyXOR(unsigned char *buff, const char *key, unsigned int inputlen, unsigned int keylen)
{
  unsigned int i;
  static unsigned int keypos = 0;
  for(i=0;i<inputlen;i++)
    {
      buff[i] ^= key[keypos++ % keylen];
    }
}

unsigned int decodeASCII(const char *input, unsigned char *destBuff, unsigned int bufflen, unsigned int inputlen)
{
  unsigned int i;
  for(i=0;i<bufflen;i++) destBuff[i] = 0;
  for(i=0;input[i]!='\0' && i < inputlen;i++) destBuff[i] = input[i];
  return i;
}

void splitline(char *input, unsigned int dist, unsigned int inputlen)
{
  int i;
  int j = 0;
  char *tmpbuff = malloc(sizeof(char)*1024);
  for(i=0;i<inputlen;i++)
    {
      if(i % dist == 0) tmpbuff[j++] = '\n';
      tmpbuff[j++] = input[i];
    }
  tmpbuff[j] = '\0';
  for(i=0;tmpbuff[i]!='\0';i++)
    input[i] = tmpbuff[i];
  free(tmpbuff);
}
