#include <stdio.h>
#include <stdlib.h>
#include "cryptutil.h"

#define BUFFSIZE 512
#define NUMSIZE 256

int main(int argc, char *argv[])
{
  char string1[BUFFSIZE];
  char string2[BUFFSIZE];
  char output[BUFFSIZE];
  unsigned char byte1[BUFFSIZE];
  unsigned char byte2[BUFFSIZE];
  int i,bufflen;
  
  for(i=0;i<BUFFSIZE;i++) byte1[i] = 0, byte2[i] = 0;

  bufflen = readTextLine(string1, BUFFSIZE);
  if( bufflen == readTextLine(string2, BUFFSIZE) )
    {
      decodeBase16(string1, byte1, BUFFSIZE, bufflen);
      decodeBase16(string2, byte2, BUFFSIZE, bufflen);
      for(i=0;i<NUMSIZE;i++)
	byte2[i] |= byte1[i];
    }
  encodeBase16(byte2, output, BUFFSIZE);
  printf("%s\n",output);

  return 0;
}
